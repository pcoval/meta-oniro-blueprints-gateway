# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"
inherit systemd

SYSTEMD_SERVICE:${PN} = "podman-icecast.service"
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_AUTO_ENABLE = "enable"

RDEPENDS:${PN} = "expect"

SRC_URI = "file://podman-icecast.service"

do_install:append () {
    install -d "${D}${systemd_system_unitdir}/"
    install -m 0644 "${WORKDIR}/podman-icecast.service" "${D}${systemd_system_unitdir}/"
}

FILES:${PN} = "${systemd_system_unitdir}"

WRITABLES = "podman-storage"
WRITABLE_TYPE[podman-storage] = "persistent"
WRITABLE_PATH[podman-storage] = "/var/lib/containers"

